FROM node:8-alpine
ENV NODE_ENV development
WORKDIR /usr/src/app
RUN apk add --no-cache curl python make g++ util-linux
RUN mkdir api supchain
COPY api/package.json api
COPY api/package-lock.json api
RUN cd api && npm install
COPY supchain/package.json supchain
COPY supchain/package-lock.json supchain
RUN cd supchain && npm install
COPY api api
COPY supchain supchain
CMD ./setup.sh && cd ../api && npm start
EXPOSE 3000
