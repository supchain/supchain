import fastify from "fastify"
import path from "path"
import fastifyStatic from "fastify-static"
import fs from "fs"

const app = fastify({ logger: true })
const indexFile = path.join(__dirname, "../client/dist/index.html")

async function boot() {
    app = 

    app.register(fastifyStatic, {
        prefix: "/static",
        root: path.join(__dirname, "../client/dist")
    })

    app.get("/*", async (request, reply) => {
        const stream = fs.createReadStream(indexFile)
        reply.type("text/html").send(stream)
    })
    await app.listen(8080)
}

boot()
    .catch(e => {
        app.log.error(e)
        process.exit(1)
    })
