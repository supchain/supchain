# SupChain [![pipeline status](https://gitlab.com/supchain/supchain/badges/develop/pipeline.svg)](https://gitlab.com/supchain/supchain/commits/develop)

## Guide d'installation
```bash
git clone git@gitlab.com:supchain/supchain.git
```

### Prérequis
- [Docker](https://docs.docker.com/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)
- [Node.js](https://nodejs.org/en/download/) (version < 9)
<details>
<summary>Installation sous Ubuntu</summary>

```bash
sudo apt install docker.io docker-compose
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install 8.15
```
</details>
<details>
<summary>Installation sous Arch Linux</summary>

```bash
sudo pacman -S docker docker-compose
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.11/install.sh | bash
nvm install 8.15
```
</details>

### Développement
#### Démarrer le réseau fabric
```
bash bin/setup
```

Pour annuler une précédente installation
```
rm -rf ~/.composer
docker stop $(docker ps -aq)
docker rm $(docker ps -aq)
```

#### Démarrer l'api
Dans le dossier `api`
```bash
npm install
npm start
```
#### Démarrer l'application
Dans le dossier `client`
```bash
npm install
npm run build:dev
```
Ouvrir un nouveau terminal

Dans le dossier `app`
```bash
npm install
npm start
```

#### Peupler la blockchain
Dans le dossier `app`
```bash
npm run seed
```
