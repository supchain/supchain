import Vue from "vue"
import Vuex from "vuex"

Vue.use(Vuex)

const store = new Vuex.Store({
    state: {
        connected: !!localStorage.getItem("user"),
        user: localStorage.getItem("user")? JSON.parse(localStorage.getItem("user")) : {
            id: null,
            name: null,
            email: null,
            role: null,
        },
    },
    mutations: {
        setConnected(state, value) {
            state.connected = value
        },
        setUser(state, value) {
            state.user.id = (value)? value.id : null
            state.user.name = (value)? value.name : null
            state.user.email = (value)? value.email : null
            state.user.role = (value)? value.role : null
        },
    },
    actions: {
        login(context, user) {
            context.commit("setConnected", true)
            context.commit("setUser", user)
            localStorage.setItem("user", JSON.stringify(user))
        },
        logout(context) {
            context.commit("setConnected", false)
            context.commit("setUser", null)
            localStorage.removeItem("user")
        },
    }
})

export default store
