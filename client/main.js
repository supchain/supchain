import "@babel/polyfill"
import Vue from "vue"
import Supchain from "./components/Supchain"
import VueRouter from "vue-router"
import store from "./store"
import routes from "./routes"

Vue.use(VueRouter)

const router = new VueRouter({
    mode: "history",
    routes,
    linkActiveClass: "active",
    linkExactActiveClass: "exact-active",
})

const app = new Vue({
    router,
    store,
    render: h => h(Supchain)
})

app.$mount("#appMountPoint")
