import Recruiter from "./components/Recruiter"
import School from "./components/School"
import Home from "./components/Home"
import AddCertificate from "./components/AddCertificate"
import AddDiploma from "./components/AddDiploma"
import Student from "./components/Student"
import Login from "./components/Login"
import SchoolCertificates from "./components/SchoolCertificates"
import SchoolDiplomas from "./components/SchoolDiplomas"
import StudentEdit from "./components/StudentEdit"

const routes = [
    {
        name: "home",
        path: "/",
        component: Home
    },
    {
        name: "school",
        path: "/school",
        component: School,
        children: [
            {
                name: "school-certificate",
                path: "/school/certificates",
                component: SchoolCertificates
            },
            {
                name: "school-diplomas",
                path: "/school/diplomas",
                component: SchoolDiplomas
            },
        ]
    },
    {
        name: "recruiter",
        path: "/recruiter",
        component: Recruiter
    },
    {
        name: "add-certificate",
        path: "/school/certificates/add",
        component: AddCertificate
    },
    {
        name: "add-diploma",
        path: "/school/diplomas/add",
        component: AddDiploma
    },
    {
        name: "student",
        path: "/student",
        component: Student
    },
    {
        name: "login",
        path: "/login",
        component: Login
    },
    {
        name: "studentedit",
        path: "/studentedit",
        component: StudentEdit
    }
]

export default routes
