import fs from "fs-extra"
import path from "path"
import {BusinessNetworkConnection} from "composer-client"

async function init(app) {
    fs.ensureDir(path.join(__dirname, "data/diplomas"))
    app.bizNetworkConnection = new BusinessNetworkConnection()

    app.businessNetworkDefinition = await app.bizNetworkConnection.connect('admin@supchain')
    app.factory = app.businessNetworkDefinition.getFactory()
    app.registry = {
        certificate: await app.bizNetworkConnection.getAssetRegistry("consortium.supchain.assets.Certificate"),
        diploma: await app.bizNetworkConnection.getAssetRegistry("consortium.supchain.assets.Diploma"),
    }
    console.log("Successfully connected to blockchain network")
}

export default init
