import path from "path"
import fileUpload from "express-fileupload"
import cookieSession from "cookie-session"
import bodyParser from "body-parser"
import cors from "cors"
import express  from "express"
import routes from "./routes"
import init from "./init"
import passport from "passport"

const publicRoot = path.join(__dirname, "../client/dist")

// Creating an express instance
const app = express()

app.use(cors())

app.use(fileUpload())

const APP_PORT = 8080
const LocalStrategy = require('passport-local').Strategy

app.use('/static', express.static(publicRoot))

app.use(bodyParser.json())
app.use (cookieSession({
    name: "supchain_session",
    keys: ['vueauthrandomkey'],
    maxAge: 24 * 60 * 60 * 1000 // 24 hours
}))

app.use(passport.initialize())
app.use(passport.session())

const authMiddleware = (req, res, next) => {
    if (!req.isAuthenticated()) {
        res.status(401).send('You are not authenticated')
    } else {
        return next()
    }
}


let users = [
  {
        id: 1,
        name: "Maxime",
        email: "mchareyron@enseirb-matmeca.fr",
        password: "supchain",
        role: "student",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
    {
        id: 2,
        name: "Marie",
        email: "mostertag@enseirb-matmeca.fr",
        password: "supchain2",
        role: "student",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
    {
        id: 3,
        name: "Abel",
        email: "abel@abel",
        password: "a",
        role: "student",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
    {
        id: 4,
        name: "enseirb",
        email: "enseirb@enseirb",
        password: "enseirb",
        role: "school",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
    {
        id: 5,
        name: "e",
        email: "e",
        password: "e",
        role: "student",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    }

]

app.post("/api/login", (req, res, next) => {
    console.log("request for login")
    passport.authenticate("local", (err, user, info) => {
        if (err) {
            return next(err)
        }

        if (!user) {
            return res.status(400).send([user, "Cannot log in" + user, info])
        }

        req.login(user, err => {
            console.log("user logged in !")
            res.send({
                id: user.id,
                name: user.name,
                email: user.email,
                role: user.role
            })
        })
    })(req, res, next)
})

app.get("/api/logout", function(req, res) {
    req.logout()

    console.log("logged out")

    return res.send()
})

app.get("/api/user", authMiddleware, (req, res) => {
    let user = users.find(user => {
        return user.id === req.session.passport.user
    })

    console.log([user, req.session])

    res.send({ user: user })
})

app.post("/api/user", (req, res) => {
  let iduser = users.findIndex( user => user.id === req.session.passport.user)
    users.splice(iduser, 1, req.body.user);
    res.send("OK")
    })

passport.use(
    new LocalStrategy(
        {
            usernameField: "email",
            passwordField: "password"
        },

        (username, password, done) => {
            let user = users.find((user) => {
                return user.email === username && user.password === password
            })

            if (user) {
                done(null, user)
            } else {
                done(null, false, { message: 'Incorrect username or password'})
            }
        }
    )
)

passport.serializeUser((user, done) => {
    done(null, user.id)
})

routes(app)

app.get("/*", (req, res, next) => {
    res.sendFile("index.html", { root: publicRoot })
})

init(app)
    .then(() => {
        app.listen(APP_PORT, () => {
            console.log(`Supchain app listening on port ${APP_PORT}`)
        })
    })
    .catch(e => {
        console.error("Error initializing app")
        console.error(e)
    })
