import axios from "axios"
import faker from "faker"
import uniqid from "uniqid"
import util from "util"
import school from "./school"
import diploma from "./diploma"
import fs from "fs-extra"
import FormData from 'form-data'
import {BusinessNetworkConnection} from "composer-client"
import { PDFDocumentFactory, PDFDocumentWriter, StandardFonts, drawText } from "pdf-lib"



function generate_pdf_bytes() {
    const pdfDoc = PDFDocumentFactory.create()
    const [timesRomanRef, timesRomanFont] = pdfDoc.embedStandardFont(
        StandardFonts.TimesRoman,
    )

    const page = pdfDoc
    .createPage([350, 500])
    .addFontDictionary('TimesRoman', timesRomanRef)

    const contentStream = pdfDoc.createContentStream(
        drawText(timesRomanFont.encodeText(`Diplôme ingénieur`), {
            x: 50,
            y: 450,
            size: 15,
            font: 'TimesRoman',
            colorRgb: [0, 0, 0.1],
        }),
        drawText(timesRomanFont.encodeText(`${faker.name.firstName()} ${faker.name.lastName()}`), {
            x: 50,
            y: 430,
            size: 11,
            font: 'TimesRoman',
            colorRgb: [0, 0, 0],
        }),
        drawText(timesRomanFont.encodeText(`le 20 février 2019`), {
            x: 250,
            y: 450,
            size: 7,
            font: 'TimesRoman',
            colorRgb: [0, 0, 0],
        }),
    )
    page.addContentStreams(pdfDoc.register(contentStream))
    pdfDoc.addPage(page)
    const pdfBytes = PDFDocumentWriter.saveToBytes(pdfDoc)
    return pdfBytes
}


async function test_add_certificate() {
    console.log('--TEST_ADD_CERTIFICATE--')

    const bizNetworkConnection = new BusinessNetworkConnection()
    const businessNetworkDefinition = await bizNetworkConnection.connect('admin@supchain')
    console.log("successfully connected to blockchain network")

    const factory = businessNetworkDefinition.getFactory()

    //Creating and adding fake schools and diplomas
    const schools = await school(bizNetworkConnection, factory)
    const diplomas = await diploma(bizNetworkConnection, factory, schools)

    //Creating 5 fake pdf files
    for( var i = 0; i < 5; i++) {
        await fs.writeFile("../data/fake/foo"+i+".pdf", generate_pdf_bytes())
    }
    console.log('+ creating 5 fake pdf files');

    //Adding certificates to the blockchain
    try
    {
        for (var i=0 ;i<diplomas.length; i++) {
            const formData = new FormData()
            const diplomaBuffer = await fs.readFile(`../data/fake/foo`+i+`.pdf`)
            formData.append("diploma", diplomaBuffer, 'foo'+i+'.pdf')
            // formData.append('diploma', new Blob());
            formData.append("diplomaId", diplomas[i])

            const response = await axios
            .post(`http://localhost:8080/certificates`, {
                body: formData
            })

            // const response = await axios
            // .post(`http://localhost:8080/certificates`
            //      , formData
            //      , {headers: formData.getHeaders()})
            var multer  = require('multer')
            var upload = multer()

            // const res = await axios.post(`http://localhost:8080/certificates`, formData);

            // const res = axios.post('http://localhost:8080/certificates'
            //                   , formData);
                              // , {headers: {'Content-Type': 'multipart/form-data'}});


            // axios({
            //    method: 'post',
            //    processData: false,
            //    contentType: 'multipart/form-data',
            //    cache: false,
            //    url: 'http://localhost:8080/certificates',
            //    data: formData,
            //    config: { headers: formData.getHeaders() }
            //    // config: { headers: {'Content-Type': 'multipart/form-data' }}
            // })

            // axios({
            //     method: 'post',
            //     url: 'myurl',
            //     data: formData,
            //     config: { headers: {'Content-Type': 'multipart/form-data' }}
            //     })
            //     .then(function (response) {
            //         //handle success
            //         console.log(response);
            //     })
            //     .catch(function (response) {
            //         //handle error
            //         console.log(response);
            //     });

            // if (response.status == '400') {
            //     throw new Error("- test_add_certificate failed : fail in adding the certificates")
            // }
        }
    }
    catch(e)
    {
        console.log('Error calling the server')
        process.exit(1)
    }

    console.log('TEST_ADD_CERTIFICATE PASSED')
}

export async function seed() {
    await test_add_certificate()
}

seed().catch(e => {
        console.log(e)
        process.exit(1)
    })

export default seed
