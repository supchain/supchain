import faker from "faker"
import uniqid from "uniqid"

async function seed(bizNetworkConnection, factory, schools) {
    const diplomaRegistry = await bizNetworkConnection.getAssetRegistry("consortium.supchain.assets.Diploma")

    //removing the existing diplomas
    const existing_diplomas = (await diplomaRegistry.getAll()).map(diploma => diploma.id)
    if (existing_diplomas.length) {
        await diplomaRegistry.removeAll(existing_diplomas)
        console.log(`- removed ${existing_diplomas.length} existing diplomas from blockchain`)
    }

    //adding 5 diplomas corresponding to schools
    const ids = new Array(schools.length)
    for (let i = 0; i < schools.length; i++) {
        ids[i] = uniqid()
    }
    const diplomas = []

    for(let id = 0; id < schools.length; id++) { // add 1 diploma per school
        const diploma = factory.newResource("consortium.supchain.assets", "Diploma", ids[id])
        diploma.school = factory.newRelationship("consortium.supchain.participants", "School", `${schools[id]}`)
        diploma.description = faker.random.arrayElement(["Ingénieur en informatique", "Ingénieur en électronique", "Ingénieur"])
        diplomas.push(diplomaRegistry.add(diploma))
    }

    await Promise.all(diplomas)
    console.log(`+ added ${diplomas.length} diplomas to blockchain`)
    return ids
}

export default seed
