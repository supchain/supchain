import uniqid from "uniqid"
import {BusinessNetworkConnection} from "composer-client"
import school from "./school"
import diploma from "./diploma"
import faker from "faker"


async function connect() {
    const bizNetworkConnection = new BusinessNetworkConnection()
    const businessNetworkDefinition = await bizNetworkConnection.connect('admin@supchain')
    console.log("successfully connected to blockchain network")
    return {
        bizNetworkConnection,
        businessNetworkDefinition
    }
}


async function test_add_school() {
    console.log('--TEST_ADD_SCHOOL--')

    //Connection
    const connection = await connect()
    const bizNetworkConnection = connection.bizNetworkConnection
    const businessNetworkDefinition = connection.businessNetworkDefinition


    //Create a new school and add it
    const schoolRegistry = await bizNetworkConnection.getParticipantRegistry("consortium.supchain.participants.School")
    const factory = businessNetworkDefinition.getFactory()
    const id = uniqid()
    const school = factory.newResource("consortium.supchain.participants", "School", `${id}`)
    school.name = "ENSEIRB-MATMECA"
    await schoolRegistry.add(school)
    console.log('- 1 school created in the blockchain')

    //Verifying if the school is added
    const resource = await schoolRegistry.get(id);
    if (resource.id !== id || resource.name !== "ENSEIRB-MATMECA") {
        throw new Error("- test_add_school failed")
    }
    console.log('- adding of the school verified')
    await bizNetworkConnection.disconnect()
}

async function test_remove_school() {
    console.log('--TEST_REMOVE_SCHOOL--')

    //Connection
    const connection = await connect()
    const bizNetworkConnection = connection.bizNetworkConnection
    const businessNetworkDefinition = connection.businessNetworkDefinition

    //Remove all the existing schools
    const schoolRegistry = await bizNetworkConnection.getParticipantRegistry("consortium.supchain.participants.School")

    const existing_schools = (await schoolRegistry.getAll()).map(school => school.id)
    if (existing_schools.length) {
        await schoolRegistry.removeAll(existing_schools)
        console.log(`- ${existing_schools.length} existing schools removed from blockchain`)
    }

    console.log('- removing of the school verified')
    await bizNetworkConnection.disconnect()
}

async function test_add_diploma() {
    console.log('--TEST_ADD_DIPLOMA--')

    //Connection
    const connection = await connect()
    const bizNetworkConnection = connection.bizNetworkConnection
    const businessNetworkDefinition = connection.businessNetworkDefinition
    const factory = businessNetworkDefinition.getFactory()
    const diplomaRegistry = await bizNetworkConnection.getAssetRegistry("consortium.supchain.assets.Diploma")

    //Creating and adding schools
    const schools = await school(bizNetworkConnection, factory)

    //adding 5 diplomas corresponding to schools
    const ids = new Array(schools.length)
    for (let i = 0; i < schools.length; i++) {
        ids[i] = uniqid()
    }
    const diplomas = []

    for(let id = 0; id < schools.length; id++) { // add 1 diploma per school
        const diploma = factory.newResource("consortium.supchain.assets", "Diploma", ids[id])
        diploma.school = factory.newRelationship("consortium.supchain.participants", "School", `${schools[id]}`)
        diploma.description = faker.random.arrayElement(["Ingénieur en informatique", "Ingénieur en électronique", "Ingénieur"])
        diplomas.push(diplomaRegistry.add(diploma))
    }

    console.log('- adding diplomas verified')
    // await bizNetworkConnection.disconnect()

}

async function test_remove_diploma() {
    console.log('--TEST_REMOVE_DIPLOMA--')

    //Connection
    const connection = await connect()
    const bizNetworkConnection = connection.bizNetworkConnection
    const businessNetworkDefinition = connection.businessNetworkDefinition

    const diplomaRegistry = await bizNetworkConnection.getAssetRegistry("consortium.supchain.assets.Diploma")
        console.log('Here');
    //removing the existing diplomas
    const existing_diplomas = (await diplomaRegistry.getAll()).map(diploma => diploma.id)
    if (existing_diplomas.length) {

        await diplomaRegistry.removeAll(existing_diplomas)
        console.log(`- removed ${existing_diplomas.length} existing diplomas from blockchain`)
    }

    console.log('- removing diplomas verified');
    await bizNetworkConnection.disconnect()

}

async function seed() {
    await test_add_diploma()
    await test_remove_diploma()
    await test_add_school()
    await test_remove_school()
}


seed().catch(e => {
        console.log(e)
        process.exit(1)
    })

// export default seed
