import { PDFDocumentFactory, PDFDocumentWriter, StandardFonts, drawText } from "pdf-lib"
import fs from "fs-extra"

export async function sign(filename, signature) {
    const original = await fs.readFile(filename)
    const pdfDoc = PDFDocumentFactory.load(original)

    const [helveticaRef, helveticaFont] = pdfDoc.embedStandardFont(
      StandardFonts.Helvetica,
    );

    const pages = pdfDoc.getPages();
    const page  = pages[0];

    page.addFontDictionary('Helvetica', helveticaRef);

    const contentStream = pdfDoc.createContentStream(
      drawText(helveticaFont.encodeText(signature), {
        x: 25,
        y: 25,
        size: 6,
        font: 'Helvetica',
        colorRgb: [0.95, 0.26, 0.21],
      }),
    )

    page.addContentStreams(pdfDoc.register(contentStream))

    const pdfBytes = PDFDocumentWriter.saveToBytes(pdfDoc)

    await fs.writeFile(filename, pdfBytes)
}

export default {
    sign,
}
