
import shajs from "sha.js"
import fs from "fs"
import util from "util"
import hasha from "hasha"

const readFile = util.promisify(fs.readFile)

export async function hash(file) {
    const contents = await readFile(file);
    const hash_value = shajs("sha256").update(contents).digest("hex");
    return hash_value
}

export function hashBuffer(content) {
    return hasha(content, {algorithm: "sha256"})
}

export async function verify(bizNetworkConnection, content) {
    const hash_value = hashBuffer(content)
    console.log("diploma hash", hash_value)
    const query = bizNetworkConnection.buildQuery('SELECT consortium.supchain.assets.Certificate WHERE (hashInfo == _$inputValue)')
    const assets = await bizNetworkConnection.query(
        query,
        {inputValue: hash_value}
    )

    return assets.length > 0
}

export default {
    hash,
    verify,
    hashBuffer,
}
// async function main() {
//     await init()
//     var result = await verify('./test2.pdf')
//     console.log(result);
// }
//
// main()
//     .catch(e => {
//         console.log(e);
//         process.exit(1)
//     })
