import uniqid from "uniqid"

async function seed(bizNetworkConnection, factory) {
    const schoolRegistry = await bizNetworkConnection.getParticipantRegistry("consortium.supchain.participants.School")

    const existing_schools = (await schoolRegistry.getAll()).map(school => school.id)
    if (existing_schools.length) {
        await schoolRegistry.removeAll(existing_schools)
        console.log(`- removed ${existing_schools.length} existing schools from blockchain`)
    }

    const schoolNames = ["ENSEIRB-MATMECA", "ENSCBP", "ENSC", "ENSEGID", "ENSTBB"]
    const schools = []
    const ids = new Array(schoolNames.length)
    for (let i = 0; i < ids.length; i++) {
        ids[i] = uniqid()
    }

    for(let i = 0; i < schoolNames.length; i++) {
        const school = factory.newResource("consortium.supchain.participants", "School", `${ids[i]}`)
        school.name = schoolNames[i]
        schools.push(schoolRegistry.add(school))
    }

    await Promise.all(schools)
    console.log(`+ added ${schoolNames.length} schools to blockchain`)
    return ids
}

export default seed
