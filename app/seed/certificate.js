import uniqid from "uniqid"
import sha from "sha.js"
import { PDFDocumentFactory, PDFDocumentWriter, StandardFonts, drawText } from "pdf-lib"
import util from "util"
import fs from "fs"
import faker from "faker"
import path from "path"
import verify_pdf from "../utils/verify_pdf"

const writeFile = util.promisify(fs.writeFile)

async function seed(bizNetworkConnection, factory, diplomas) {
    const pdfDoc = PDFDocumentFactory.create()
    const [timesRomanRef, timesRomanFont] = pdfDoc.embedStandardFont(
        StandardFonts.TimesRoman,
    )

    const page = pdfDoc
    .createPage([350, 500])
    .addFontDictionary('TimesRoman', timesRomanRef)

    const contentStream = pdfDoc.createContentStream(
        drawText(timesRomanFont.encodeText(`Diplôme ingénieur`), {
            x: 50,
            y: 450,
            size: 15,
            font: 'TimesRoman',
            colorRgb: [0, 0, 0.1],
        }),
        drawText(timesRomanFont.encodeText(`${faker.name.firstName()} ${faker.name.lastName()}`), {
            x: 50,
            y: 430,
            size: 11,
            font: 'TimesRoman',
            colorRgb: [0, 0, 0],
        }),
        drawText(timesRomanFont.encodeText(`le 20 février 2019`), {
            x: 250,
            y: 450,
            size: 7,
            font: 'TimesRoman',
            colorRgb: [0, 0, 0],
        }),
    )

    page.addContentStreams(pdfDoc.register(contentStream))

    pdfDoc.addPage(page)

    const pdfBytes = PDFDocumentWriter.saveToBytes(pdfDoc)

    await writeFile("data/fake/foo.pdf", pdfBytes)

    console.log(`+ generated a fake diploma in '${path.join(__dirname, "../data/fake/foo.pdf")}'`)

    const certificateRegistry = await bizNetworkConnection.getAssetRegistry("consortium.supchain.assets.Certificate")

    // Retire tous les diplêmes déja éxistants
    const existing_certificates = (await certificateRegistry.getAll()).map(certificate => certificate.id)
    if (existing_certificates.length) {
        await certificateRegistry.removeAll(existing_certificates)
        console.log(`- removed ${existing_certificates.length} existing certificates`)
    }

    const certificates = []

    // for(let diploma of diplomas) {
    //     const certificate = factory.newResource("consortium.supchain.assets", "Certificate", uniqid())
    //     certificate.diploma = factory.newRelationship("consortium.supchain.assets", "Diploma", diploma)
    //     certificate.hashInfo =  await verify_pdf.hash(path.join(__dirname, "../data/fake/foo.pdf"))
    //     certificates.push(certificateRegistry.add(certificate))
    // }
    //
    // await Promise.all(certificates)
    console.log(`+ added ${certificates.length} certificates to blockchain`)
}

export default seed
