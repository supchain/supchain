import school from "./school"
import diploma from "./diploma"
import certificate from "./certificate"
import {BusinessNetworkConnection} from "composer-client"

/*
*  Remplit la blockchain avec des données de tests
*/
async function seed() {
    const bizNetworkConnection = new BusinessNetworkConnection()

    const businessNetworkDefinition = await bizNetworkConnection.connect('admin@supchain')
    console.log("successfully connected to blockchain network")

    const factory = businessNetworkDefinition.getFactory()

    const schools = await school(bizNetworkConnection, factory)
    const diplomas = await diploma(bizNetworkConnection, factory, schools)
    await certificate(bizNetworkConnection, factory, diplomas)

    await bizNetworkConnection.disconnect()
}

seed().catch(e => {
        console.log(e)
        process.exit(1)
    })
