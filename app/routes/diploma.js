export default app => {
    app.get("/diplomas", async (req, res) => {
        const diplomas = await app.registry.diploma.resolveAll()
        res.status(200).send({
            data: diplomas
        })
    })
}
