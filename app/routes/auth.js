import passport from "passport"
import {Strategy} from "passport-local"

const users = [
    {
        id: 1,
        name: "Maxime",
        email: "mchareyron@enseirb-matmeca.fr",
        password: "supchain",
        role: "student",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
    {
        id: 2,
        name: "Marie",
        email: "mostertag@enseirb-matmeca.fr",
        password: "supchain2",
        role: "student",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
    {
        id: "3",
        name: "Abel",
        email: "abel@abel",
        password: "a",
        role: "student",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
    {
        id: "1",
        name: "enseirb",
        email: "enseirb@enseirb",
        password: "enseirb",
        role: "school",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
    {
        id: "5",
        name: "e",
        email: "e",
        password: "e",
        role: "student",
        age: "20",
        diploma: "ingénieur informatique",
        adress: "Talence"
    },
]

export default app => {
    app.use(passport.initialize())
    app.use(passport.session())

    const authMiddleware = (req, res, next) => {
        if (!req.isAuthenticated()) {
            res.status(401).send('You are not authenticated')
        } else {
            return next()
        }
    }

    passport.use("local",
        new Strategy(
            {
                usernameField: "email",
                passwordField: "password"
            },

            (username, password, done) => {
                let user = users.find((user) => {
                    return user.email === username && user.password === password
                })

                if (user) {
                    done(null, user)
                } else {
                    done(null, false, { message: 'Incorrect username or password'})
                }
            }
        )
    )

    passport.serializeUser((user, done) => {
        done(null, user.id)
    })

    passport.deserializeUser((id, done) => {
        let user = users.find((user) => {
            return user.id === id
        })

        done(null, user)
    })

    app.post("/api/login", (req, res, next) => {
        console.log("request for login")
        passport.authenticate("local", (err, user, info) => {
            if (err) {
                return next(err)
            }

            if (!user) {
                return res.status(400).send([user, "Cannot log in" + user, info])
            }

            req.login(user, err => {
                console.log("user logged in !")
                res.send({
                    id: user.id,
                    name: user.name,
                    email: user.email,
                    role: user.role,
                    age: user.age,
                    diploma: user.diploma,
                    adress: user.adress
                })
            })
        })(req, res, next)
    })

    app.get("/api/logout", function(req, res) {
        req.logout()

        console.log("logged out")

        return res.send()
    })

    app.post("/api/user",passport.authenticate("local"), (req, res, next) => {
        console.log("request for user")
        // passport.authenticate("local", (err, user, info) => {
        //     if (err) {
        //         return next(err)
        //     }
        //
        //     if (!user) {
        //         return res.status(400).send([user, "Cannot log in" + user, info])
        //     }
         console.log(user)
         console.log(req.body)
         res.send("okok")
        // })(req, res, next)
    })

    app.get("/api/user", authMiddleware, (req, res) => {
        let user = users.find(user => {
            return user.id === req.session.passport.user
        })

        console.log([user, req.session])

        res.send({ user: user })
    })
}
