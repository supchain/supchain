import diploma from "./diploma"
import auth from "./auth"
import certificate from "./certificate"

export default app => {
    diploma(app)
    certificate(app)
    auth(app)
}
