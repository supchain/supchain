import uniqid from "uniqid"
import fs from "fs-extra"
import {date} from "../utils/date"
import pdf from "../utils/pdf"
import verify_pdf from "../utils/verify_pdf"
import sha from "sha.js"

const diplomaSecret = "supersecret"

export default app => {
    app.post('/certificates', async (req, res) => {
        const files = req.files

        if(!files || !files.diploma || !req.body.diplomaId) {
            res.status(400).send(new Error("No file provided"))
        }

        if(files.diploma.mimetype !== "application/pdf") {
            res.status(400).send(new Error("diploma is not a pdf"))
        }

        // We should now verifiy if the uploaded files are indeed pictures (instead of PDF's)
        // if((files.signature.mimetype !== "image/png") && (files.signature.mimetype !== "image/jpeg")) {
        //     res.status(400).send(new Error("signature is not a valid image"))
        // }

        const certificateId = uniqid()
        const outputDiploma = `data/diplomas/${certificateId}.pdf`
        // const outputSignature = `${outputDiploma}.sig.png`

        await Promise.all([
            fs.writeFile(outputDiploma, files.diploma.data),
            // fs.writeFile(outputSignature, files.signature.data)
        ])

        await pdf.sign(outputDiploma, `${sha("sha256").update(certificateId + diplomaSecret).digest("hex")} : ${date()}`)

        const diplomaBuffer = await fs.readFile(outputDiploma)
        const certificate_hash = verify_pdf.hashBuffer(diplomaBuffer)
        // console.log(diplomaBuffer)
        // console.log(certificate_hash)

        const certificateRegistry = await app.bizNetworkConnection.getAssetRegistry("consortium.supchain.assets.Certificate")
        const certificate = app.factory.newResource("consortium.supchain.assets", "Certificate", certificateId)
        certificate.diploma = app.factory.newRelationship("consortium.supchain.assets", "Diploma", req.body.diplomaId)
        certificate.hashInfo = certificate_hash
        await app.registry.certificate.add(certificate)

        res.status(200).send("ok")
    })

    app.get("/certificates", async (req, res) => {
        const certificates = await app.registry.certificate.resolveAll()
        res.status(200).send({
            data: certificates
        })
    })

    app.post('/certificates/verify', async (req, res) => {
        if(!req.files || !req.files.certificate) {
            res.status(400).send(new Error("No file provided"))
        }

        console.log(req.files.certificate.data)

        const verified = await verify_pdf.verify(app.bizNetworkConnection, req.files.certificate.data)

        if (!verified) {
            res.sendStatus(400)
        }
        else {
            res.sendStatus(200)
        }
    })
}
