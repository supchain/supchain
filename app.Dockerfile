FROM node:8-alpine
ENV NODE_ENV development
WORKDIR /usr/src/app
RUN apk add --no-cache curl python make g++ util-linux
RUN mkdir client app
COPY client/package.json client
COPY app/package.json app
RUN cd app && npm install
RUN cd client && npm install
COPY app app
COPY client client
RUN cd client && npm run build
CMD cd app && npm start
EXPOSE 8080
